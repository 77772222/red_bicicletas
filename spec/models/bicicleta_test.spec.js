var Bicicleta = require('../../models/bicicleta')

beforeEach(() => {
    Bicicleta.allBicis = []
});

describe ('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe ('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var z = new Bicicleta(1, 'azul', 'montaña', [-34.61, -58,39])
        Bicicleta.add(z);

        //le agrego una para ver si funciona el add
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(z);
    })
})

describe ('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var bici = new Bicicleta(1, 'verde', 'urbana');
        var bici2 = new Bicicleta(2, 'azul', 'urbana');

        Bicicleta.add(bici);
        Bicicleta.add(bici2);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(bici.color);
        expect(targetBici.modelo).toBe(bici.modelo);
    })
})

describe ('Bicicleta.removeId', () => {
    it('debe eliminar la bici con id 1', () => {
        var bici = new Bicicleta (1, 'verde', 'montaña');
        Bicicleta.add(bici);

        Bicicleta.removeId(bici.id);

        expect(Bicicleta.allBicis.length).toBe(0);
    })
})