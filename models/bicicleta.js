var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return 'id: ' + this.id + ' | color: ' + this.color;    
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici) {
        return aBici
    } else {
        throw new Error (`No existe la bici con el id: ${aBiciId}`)
    }
}

Bicicleta.removeId = function(aBiciId){

    /*var aBici = Bicicleta.allBicis.findById(x => x.id == aBiciId);
    Bicicleta.allBicis.splice(aBici, 1)*/


    for (var i=0; i < Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

var a = new Bicicleta (1, 'rojo', 'urbana', [-34.601242, -58.3861])

var b = new Bicicleta (2, 'blanca', 'urbana', [-34.5962, -58.3761])

var c = new Bicicleta (3, 'gris', 'urbana', [-34.5862, -58.3661])

var d = new Bicicleta (4, 'azul', 'urbana', [-34.6162, -58.3781])

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);


module.exports = Bicicleta;