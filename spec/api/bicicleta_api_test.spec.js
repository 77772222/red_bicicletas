var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www'); //estp se hace para que el servidor se ejecute al realizarse el test, no es necesario hacer el npm start
const { head } = require('request');

beforeEach(() => {
    Bicicleta.allBicis = []
});

describe('Bicicleta API', () => {
    describe('get BICICLETAS', () => {
        it('status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'negro', 'urbana');
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            }) //es necesario poner function, no funciona la flecha, y la llave tiene que estar pegada al parentesis
        })
    })

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}'; //es lo mismo que hacer el formato json y pasarlo a string (creo que eso se hace en el proyecto de angular)
            
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('rojo');
                done(); //es lo que espera jasmine para detener el test, algunas veces el test termina antes de que se realiza la peticion POST
            })
        });
    })
})